#include "vehicule.hpp"

Vehicule::Vehicule(){
}

Vehicule::Vehicule(u_char l, u_char c, u_char t, u_char o)
{
    ligne = l;
    colonne = c;
    taille = t;
    orientation = o;
}

u_char Vehicule::get_colonne() const
{
    return colonne;
}
u_char Vehicule::get_ligne() const
{
    return ligne;
}

u_char Vehicule::get_orientation() const
{
    return orientation;
}

u_char Vehicule::get_taille() const
{
    return taille;
}

void Vehicule::set_colonne(u_char c)
{
    colonne = c;
}

void Vehicule::set_ligne(u_char l)
{
    ligne = l;
}
void Vehicule::set_taille(u_char t)
{
    taille = t;
}

void Vehicule::set_orientation(u_char o)
{
    orientation = o;
}

bool Vehicule::operator==(const Vehicule &v) const
{
    return (colonne == v.colonne && ligne == v.ligne && taille == v.taille && orientation == v.orientation);
}

bool Vehicule::operator!=(const Vehicule &v) const
{
    return !(colonne == v.colonne && ligne == v.ligne && taille == v.taille && orientation == v.orientation);
}