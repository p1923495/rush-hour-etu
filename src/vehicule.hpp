#ifndef VEHICULE_HPP
#define VEHICULE_HPP
#include <iostream>
class Vehicule
{
private:
    // char: 48=0 53=5
    u_char ligne;
    u_char colonne;
    u_char taille;      // 2 ou 3
    u_char orientation; // 1= HORIZONTAL 0=VERTICAL
public:
    Vehicule();
    Vehicule(u_char l, u_char c, u_char t, u_char o);

    u_char get_ligne() const;
    u_char get_colonne() const;
    u_char get_taille() const;
    u_char get_orientation() const; //1= HORIZONTAL 0=VERTICAL

    void set_ligne(u_char ligne);
    void set_colonne(u_char colonne);
    void set_taille(u_char taille);
    void set_orientation(u_char o);

    bool operator ==(const Vehicule& v) const;
    bool operator !=(const Vehicule& v) const;
};

#endif