#include "tableau.hpp"
int main()
{
    Tableau t;

    t.generer_tableau(18);
    std::cout << t;
    t.exporter_tableau();
    u_char nb_mvt = 0;
    std::vector<mouvement> liste_mvt_sol = t.resoudre();
    for (mouvement mvt : liste_mvt_sol)
    {
        nb_mvt++;
        std::cout << std::endl
                  << "mouvement " << nb_mvt + 0 << " : (" << mvt.colonne_depart + 0 << "," << mvt.ligne_depart + 0 << ") --> (" << mvt.colonne_arrivee + 0 << "," << mvt.ligne_arrivee + 0 << ")";
        t.faire_mouvement(mvt);
        std::cout << t;
    }

    return 0;
}