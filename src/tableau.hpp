#ifndef TABLEAU_HPP
#define TABLEAU_HPP
#include "vehicule.hpp"
#include <vector>
#include <array>
#include <fstream>
#include <iostream>
#include <string>
struct mouvement // c_depart,l_depart, c_arrivee, l_arrivee
{
    // char: 48=0 53=5

    u_char colonne_depart;
    u_char ligne_depart;
    u_char colonne_arrivee;
    u_char ligne_arrivee;
};

class Tableau
{
private:
    u_char NB_VEHICULES;
    Vehicule tab_Vehicule[16]; // i==0 vehicule = cible.
    Vehicule *tiles[36];       // 0-5 == (0,0)-(5,0) 6-11 == (0,1)-(5,1)
    u_char sortie[2];          // 0=colonne 1=ligne
    std::vector<mouvement> vec_mouvements_resolution;

    std::string get_string_Vehicule() const;
    u_char get_NB_VEHICULES() const;

    std::vector<mouvement> get_mouvements_possibles() const; 
    bool is_mouvement_possible(mouvement m) const;
    Tableau new_Tableau_mouvement(mouvement mvt);// génère un nouveau tableau à partir de celui qui execute la fonction et un mouvement

    bool ajouter_vehicule(Vehicule v); // true if ajout réussi

    bool sortie_atteinte() const;                            

public:
    Tableau();
    Tableau(const Tableau &t);
    Tableau &operator=(const Tableau &t);

    void charger_tableau(const char *filePath); // set tiles to nullptr
    void exporter_tableau();

    void faire_mouvement(mouvement mvt);

    std::vector<mouvement> resoudre();
    void generer_tableau(u_int NB_COUPS = 10); // utilise résoudre et ajouter véhicule pour générer un tableau de minimum NB_COUPS coups

    friend std::ostream &operator<<(std::ostream &os, const Tableau &Tableau);//output
};

u_char get_index(u_char colonne, u_char ligne);//facilite la vie

#endif