#include "tableau.hpp"
#include <queue>
#include <unordered_set>
#include <random>

u_char get_index(u_char colonne, u_char ligne)
{
    return colonne + 6 * ligne;
}
void get_coord(u_char index, u_char &x, u_char &y)
{
    x = index % 6;
    y = (index - x) / 6;
}

Tableau::Tableau() = default;

Tableau::Tableau(const Tableau &t)
{
    NB_VEHICULES = 0;
    vec_mouvements_resolution = t.vec_mouvements_resolution;

    for (u_char i = 0; i < 36; i++)
    {
        tiles[i] = nullptr;
    }

    for (u_char i = 0; i < t.NB_VEHICULES; i++)
    {
        ajouter_vehicule(t.tab_Vehicule[i]);
    }

    sortie[0] = t.sortie[0];
    sortie[1] = t.sortie[1];
}

Tableau &Tableau::operator=(const Tableau &t)
{
    NB_VEHICULES = 0;

    vec_mouvements_resolution.clear();
    for (mouvement val : t.vec_mouvements_resolution)
    {
        vec_mouvements_resolution.push_back(val);
    }

    for (Vehicule *&tile : tiles)
    {
        tile = nullptr;
    }

    for (int i = 0; i < t.NB_VEHICULES; ++i)
    {
        this->ajouter_vehicule(t.tab_Vehicule[i]);
    }

    sortie[0] = t.sortie[0];
    sortie[1] = t.sortie[1];

    return *this;
}

void Tableau::faire_mouvement(mouvement mvt)
{
    u_char index = get_index(mvt.colonne_depart, mvt.ligne_depart);
    Vehicule *ptrVehicule = tiles[index];
    u_char taille = ptrVehicule->get_taille();
    u_char orientation = ptrVehicule->get_orientation();
    ptrVehicule->set_colonne(mvt.colonne_arrivee);
    ptrVehicule->set_ligne(mvt.ligne_arrivee);

    u_char index_travail;
    if (orientation == 1)
    {
        for (u_char k = 0; k < taille; k++)
        {
            index_travail = get_index(mvt.colonne_depart + k, mvt.ligne_depart);
            tiles[index_travail] = nullptr;
        }
        for (u_char k = 0; k < taille; k++)
        {

            index_travail = get_index(mvt.colonne_arrivee + k, mvt.ligne_arrivee);
            tiles[index_travail] = ptrVehicule;
        }
    }
    else
    {
        for (u_char k = 0; k < taille; k++)
        {
            index_travail = get_index(mvt.colonne_depart, mvt.ligne_depart + k);
            tiles[index_travail] = nullptr;
        }
        for (u_char k = 0; k < taille; k++)
        {

            index_travail = get_index(mvt.colonne_arrivee, mvt.ligne_arrivee + k);
            tiles[index_travail] = ptrVehicule;
        }
    }
}

bool Tableau::sortie_atteinte() const
{
    const Vehicule *ptrCible = tab_Vehicule;
    return (tiles[get_index(sortie[0], sortie[1])] == ptrCible);
}

std::string Tableau::get_string_Vehicule() const
{
    std::string string_vehicule = "";
    for (int i = 0; i < NB_VEHICULES; i++)
    {
        string_vehicule += tab_Vehicule[i].get_colonne() + '0';
        string_vehicule += tab_Vehicule[i].get_ligne() + '0';
        string_vehicule += tab_Vehicule[i].get_taille() + '0';
        string_vehicule += tab_Vehicule[i].get_orientation() + '0';
    }
    return string_vehicule;
}

u_char Tableau::get_NB_VEHICULES() const
{
    return NB_VEHICULES;
}

Tableau Tableau::new_Tableau_mouvement(mouvement mvt)
{
    Tableau t(*this);
    t.faire_mouvement(mvt);
    t.vec_mouvements_resolution.push_back(mvt);
    return t;
}

bool Tableau::is_mouvement_possible(mouvement mvt) const
{
    u_char index = get_index(mvt.colonne_depart, mvt.ligne_depart);
    u_char index_cible;

    Vehicule *ptrVehicule = tiles[index];

    u_char taille = ptrVehicule->get_taille();
    u_char orientation = ptrVehicule->get_orientation();
    if (orientation == 1)
    {
        for (u_char k = 0; k < taille; k++)
        {
            index_cible = get_index(mvt.colonne_arrivee + k, mvt.ligne_arrivee);

            if (tiles[index_cible] && tiles[index_cible] != ptrVehicule)
            {
                return false;
            }
        }
    }
    else
    {
        for (u_char k = 0; k < taille; k++)
        {
            index_cible = get_index(mvt.colonne_arrivee, mvt.ligne_arrivee + k);

            if (tiles[index_cible] && tiles[index_cible] != ptrVehicule)
            {
                return false;
            }
        }
    }

    return true;
}

std::vector<mouvement> Tableau::get_mouvements_possibles() const
{
    std::vector<mouvement> liste_mvt_poss;
    u_char colonne_arrivee, ligne_arrivee;
    u_char taille, horizontal;
    bool stop_direction1, stop_direction2;
    mouvement mvt;
    u_char k;
    for (u_char i = 0; i < NB_VEHICULES; i++)
    {
        stop_direction1 = false;
        stop_direction2 = false;

        horizontal = tab_Vehicule[i].get_orientation(); // 1= horizontal 0=vertical
        const u_char ligne_depart = tab_Vehicule[i].get_ligne();
        taille = tab_Vehicule[i].get_taille();
        const u_char colonne_depart = tab_Vehicule[i].get_colonne();
        k = 1;
        while (!stop_direction1)
        {

            colonne_arrivee = colonne_depart + horizontal * k;
            if (colonne_arrivee + taille * horizontal > 6)
            {
                break;
            }
            ligne_arrivee = ligne_depart + (1 - horizontal) * k;
            if (ligne_arrivee + (1 - horizontal) * taille > 6)
            {
                break;
            }
            mvt = {colonne_depart, ligne_depart, colonne_arrivee, ligne_arrivee};

            k++;
            if (is_mouvement_possible(mvt))
            {
                liste_mvt_poss.push_back(mvt);
            }
            else
            {
                stop_direction1 = true;
            }
        }
        k = 1;
        while (!stop_direction2)
        {
            colonne_arrivee = colonne_depart - horizontal * k;
            if (colonne_arrivee > 6)
            {
                break;
            }
            ligne_arrivee = ligne_depart - (1 - horizontal) * k;
            if (ligne_arrivee > 6)
            {
                break;
            }
            mvt = {colonne_depart, ligne_depart, colonne_arrivee, ligne_arrivee};

            k++;
            if (is_mouvement_possible(mvt))
            {
                liste_mvt_poss.push_back(mvt);
            }
            else
            {
                stop_direction2 = true;
            }
        }
    }

    return liste_mvt_poss;
}

bool Tableau::ajouter_vehicule(Vehicule v)
{

    if (NB_VEHICULES == 16)
    {
        return false;
    }

    u_char colonne, ligne, taille, horizontal;
    horizontal = v.get_orientation(); // 1= horizontal 0=vertical
    ligne = v.get_ligne();
    taille = v.get_taille();
    colonne = v.get_colonne();

    for (u_char k = 0; k < taille; k++)
    {
        if (tiles[get_index(colonne, ligne)])
        {
            return false;
        }
        colonne = colonne + horizontal;
        ligne = ligne + (1 - horizontal);
    }
    colonne = v.get_colonne();
    ligne = v.get_ligne();
    tab_Vehicule[NB_VEHICULES] = v;
    for (u_char k = 0; k < taille; k++)
    {
        tiles[get_index(colonne, ligne)] = &tab_Vehicule[NB_VEHICULES];
        colonne = colonne + horizontal;
        ligne = ligne + (1 - horizontal);
    }

    NB_VEHICULES++;

    return true;
}

void Tableau::charger_tableau(const char *filepath)
{

    std::ifstream fichier(filepath, std::ios::in);
    for (int i = 0; i < 36; i++)
    {
        tiles[i] = nullptr;
    }

    if (fichier)
    {
        int ligneSortie, colonneSortie;
        fichier >> ligneSortie >> colonneSortie;
        sortie[1] = ligneSortie;
        sortie[0] = colonneSortie;
        NB_VEHICULES = 0;
        int ligne, colonne, taille, orientation;
        while (fichier >> ligne >> colonne >> taille >> orientation)
        {
            ajouter_vehicule(Vehicule(ligne, colonne, taille, orientation));
        }
        fichier.close();

        return;
    }
}

std::vector<mouvement> Tableau::resoudre()
{

    std::queue<Tableau> fifoTableau;
    std::unordered_set<std::string> liste_Tableaux_visites; //

    fifoTableau.push(*this);
    liste_Tableaux_visites.insert(this->get_string_Vehicule());
    Tableau tableau_travail;
    Tableau tableau_suivant;
    while (!fifoTableau.empty())
    {
        tableau_travail = fifoTableau.front();
        std::vector<mouvement> liste_mouvements_possibles = tableau_travail.get_mouvements_possibles();

        for (mouvement mvt : liste_mouvements_possibles)
        {
            tableau_suivant = tableau_travail.new_Tableau_mouvement(mvt);

            if (tableau_suivant.sortie_atteinte())
            {
                return tableau_suivant.vec_mouvements_resolution;
            }

            std::string string_suivant = tableau_suivant.get_string_Vehicule();
            if (liste_Tableaux_visites.find(string_suivant) == liste_Tableaux_visites.end())
            {
                liste_Tableaux_visites.insert(string_suivant);
                fifoTableau.push(tableau_suivant);
            }
        }
        fifoTableau.pop();
    }
        return std::vector<mouvement>();
    
}

std::ostream &operator<<(std::ostream &os, const Tableau &tableau)
{
    os << std::endl;
    for (u_char ligne = 0; ligne < 6; ligne++)
    {
        os << "|";
        for (u_char colonne = 0; colonne < 6; colonne++)
        {
            if (!tableau.tiles[get_index(colonne, ligne)])
            {
                os << " |";
            }
            else
            {
                if (tableau.tiles[get_index(colonne, ligne)]->get_orientation() == 0)
                {

                    os << "!|";
                }
                else
                {
                    os << "-|";
                }
            }
        }
        os << std::endl;
    }
    return os;
}

void Tableau::generer_tableau(u_int NB_COUPS)
{
    int SAFETY = 0;
    do
    {
        std::cout << std::endl
                  << "GENERATION TABLEAU DE MIN" << NB_COUPS << " COUPS" << std::endl;
        Tableau tab;
        NB_VEHICULES = 0;
        sortie[0] = 5;
        sortie[1] = 2;
        for (int k = 0; k < 36; k++)
        {
            tiles[k] = nullptr;
        }
        ajouter_vehicule(Vehicule(2, 0, 2, 1));
        std::random_device rd;
        u_char colonne, ligne, taille, orientation, resultat;
        u_char A_BATTRE = resoudre().size();

        SAFETY = 0;
        while (A_BATTRE < NB_COUPS && SAFETY < 100)
        {
            do
            {
                tab = *this;
                taille = rd() % 2 + 2;
                orientation = rd() % 2;
                colonne = rd() % (6 - ((taille - 1) * (orientation)));
                ligne = rd() % (6 - ((taille - 1) * (1 - orientation)));
                SAFETY++;
            } while (SAFETY < 100 && ((ligne == 2 && orientation == 1) || !tab.ajouter_vehicule(Vehicule(ligne, colonne, taille, orientation))));
            resultat = tab.resoudre().size();
            if (resultat > A_BATTRE)
            {
                A_BATTRE = resultat;
                std::cout << A_BATTRE + 0 << " coups!" << std::endl;
                ajouter_vehicule(Vehicule(ligne, colonne, taille, orientation));
            }
        }
    } while (SAFETY == 100);
}

void Tableau::exporter_tableau()
{
    std::ofstream my_file;
    std::cout << std::endl
              << "EXPORTATION DANS bin/tableau_genere.txt" << std::endl;
    my_file.open("bin/tableau_genere.txt");
    if (my_file.is_open())
    {
        my_file << (int)sortie[1] << " " << (int)sortie[0] << "\n";
        for (u_char i = 0; i < NB_VEHICULES; i++)
        {
            my_file << (int)tab_Vehicule[i].get_ligne() << " " << (int)tab_Vehicule[i].get_colonne() << " " << (int)tab_Vehicule[i].get_taille() << " " << (int)tab_Vehicule[i].get_orientation() << "\n";
        }
        my_file.close();
    }
    else
    {
        std::cout << std::endl
                  << "Exportation ratee" << std::endl;
    }
}