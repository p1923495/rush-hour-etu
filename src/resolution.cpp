#include "tableau.hpp"

int main()
{
    Tableau t;

    t.charger_tableau("Sujet/puzzle.txt");
    std::cout << t;

    std::vector<mouvement> liste_mouvements_solution = t.resoudre();
    u_char nb_mvt = 0;
    for (mouvement mvt : liste_mouvements_solution)
    {
        nb_mvt++;
        std::cout << std::endl
                  << "mouvement " << nb_mvt + 0 << " : (" << mvt.colonne_depart + 0 << "," << mvt.ligne_depart + 0 << ") --> (" << mvt.colonne_arrivee + 0 << "," << mvt.ligne_arrivee + 0 << ")";
        t.faire_mouvement(mvt);
        std::cout << t;
    }

    return 0;
}