# Projet Rush Hour

## INSTRUCTIONS COMPILATION ET EXECUTION:

make compile les exécutables de résolution et de génération dans le répertoire bin.

make clean permet d'effacer les fichiers .o de compilation, les exécutables, et le fichier exporté par la génération.

Pas de paramètres à passer pour l'exécution.

### __ fonctionnement bin/resolution:__

Cet exécutable charge, puis résout le fichier "Sujet/puzzle.txt".
Après avoir obtenu la liste des coups nécessaires à la résolution, il les joue un par un en montrant clairement le coup joué et la configuration du tableau après le coup.

Le chargement de configuration présuppose une configuration initiale valide (pas de véhicules qui se chevauchent etc)

Pour l'affichage des configurations de tableaux, ça passe par la console. On ne différencie pas les véhicules les uns des autres, simplement leur orientation. En effet, comme on ne joue pas au jeu mais laisse la machine jouer, il importe peu de voir les véhicules distinctement les uns des autres, juste de voir qu'ils se déplacent bien de la bonne façon.
Un véhicule horizontal sera représenté par une série de - dans le cadrillage, un véhicule vertical par des ! et si rien n'occupe une case elle est représentée comme vide.

Pour la résolution, le tableau va faire un parcours en largeur des situations possibles à partir de la configuration initiale, afin de trouver la solution qui demande le minimum de coups.

Pour trouver les tableaux suivants, on obtient les mouvements possibles à partir de la situation actuelle, puis on génère un tableau par mouvement possible.

Pour faciliter la recherche de la solution, on stocke les configurations qu'on visite, et on ne réétudie pas une configuration déjà étudiée.

### __fonctionnement bin/generation:__

Cet exécutable génère selon le nombre passé en paramètre dans la fonction t.generer_tableau(param) un tableau qui a besoin de *minimum* param coups pour être résolu.
Il l'exporte ensuite dans le fichier bin/tableau_genere.txt, le créant si nécessaire.
Il le résoud ensuite visuellement comme dans bin/resolution

La génération est loquace, indiquant là où elle en est par le nombre de coups requis au point où elle en est dans la génération.

J'ai essayé de faire une génération intelligente, réfléchi à trouver des algorithmes itératifs de génération, par exemple: 

* Ajouter un véhicule vertical à la sortie, puis un horizontal qui en bloque le mouvement puis un vertical qui bloque le mouvement du second, faisant une chaîne simple mais pas des tableaux variés ou intéressants.
* Partir d'un tableau pré-généré et rajouter des véhicules pour le complexifier.

Au final, j'ai décidé que comme ma résolution était extrêmement rapide du fait de l'optimisation (ressenti instantané même pour 25 mouvements) je pouvais me permettre de faire la chose de façon brute.
Je génère donc aléatoirement des véhicules, et les ajoute uniquement si ils sont intéressants (rajoutent des coups à ma configuration actuelle et nouvelle configuration résoluble).

Du fait de l'aléatoire, la génération peut être intéressante ou pas intéressante du tout (imaginons une ligne remplie de véhicules horizontaux, rien ne pourrait bouger donc ça ne serait pas intéressant). Il est aussi possible de ne pas pouvoir ajouter de véhicules à la configuration. Pour pallier à ça, si le programme bloque trop de fois à tenter de rajouter un véhicule, il recommence la génération depuis le début.