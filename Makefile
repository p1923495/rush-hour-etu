CC					= g++
LD 					= g++
LDFLAGS  			=
CPPFLAGS 			= -Wall -ggdb -std=c++11 -pedantic  #-O2   # pour optimiser
OBJ_DIR 			= obj
SRC_DIR 			= src
BIN_DIR 			= bin

SOURCES = 
SOURCES += vehicule.cpp
SOURCES += tableau.cpp

SOURCES_TEST= $(SOURCES) generation.cpp
SOURCES_TARGET=$(SOURCES) resolution.cpp

TEST = generation
TARGET = resolution


default: make_dir_obj make_dir_bin $(BIN_DIR)/$(TEST) $(BIN_DIR)/$(TARGET)

make_dir_obj:
	test -d $(OBJ_DIR) || mkdir -p $(OBJ_DIR)


make_dir_bin:
	test -d $(BIN_DIR) || mkdir -p $(BIN_DIR)


$(BIN_DIR)/$(TEST): $(SOURCES_TEST:%.cpp=$(OBJ_DIR)/%.o)
	$(LD) $+ -o $@ $(LDFLAGS)

$(BIN_DIR)/$(TARGET): $(SOURCES_TARGET:%.cpp=$(OBJ_DIR)/%.o)
	$(LD) $+ -o $@ $(LDFLAGS) 
	
$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CC) -c $(CPPFLAGS) $< -o $@


########## nettoyage ##########
clean:
	rm -rf $(OBJ_DIR) $(BIN_DIR)/$(TARGET) $(BIN_DIR)/$(TEST)  $(BIN_DIR)/*.txt

